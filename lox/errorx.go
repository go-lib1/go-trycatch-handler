package lox

func Expect(f func() any) (any, any) {
	var val any

	err, _ := TryWithError(func() {
		val = f()
	})

	return val, err
}

func Throw(err any) {
	panic(err)
}
