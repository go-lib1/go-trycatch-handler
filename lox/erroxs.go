package lox

import "github.com/samber/lo"

func panicArgs(err error, args ...any) {
	if err == nil {
		return
	}

	switch len(args) {
	case 0:
		panic(err)
	case 1:
		panic(lo.T2(err, args[0]))
	case 2:
		panic(lo.T3(err, args[0], args[1]))
	case 3:
		panic(lo.T4(err, args[0], args[1], args[2]))
	case 4:
		panic(lo.T5(err, args[0], args[1], args[2], args[3]))
	case 5:
		panic(lo.T6(err, args[0], args[1], args[2], args[3], args[4]))
	case 6:
		panic(lo.T7(err, args[0], args[1], args[2], args[3], args[4], args[5]))
	}
}

func panicErr(err error, f func(error)) {
	if err == nil {
		return
	}
	if f != nil {
		f(err)
	}
	panic(err)
}

type mustX0 struct {
	err error
}

func MustX0(err error) *mustX0 {
	return &mustX0{
		err: err,
	}
}

func (m *mustX0) OrErr(args ...any) {
	panicArgs(m.err, args...)
}

func (m *mustX0) OrErrSlice(args []any) {
	m.OrErr(args...)
}

func (m *mustX0) OrErrFn(f func(error)) {
	panicErr(m.err, f)
}

type mustX1[T any] struct {
	val T
	err error
}

func MustX1[T any](val T, err error) *mustX1[T] {
	return &mustX1[T]{
		val: val,
		err: err,
	}
}

func (m *mustX1[T]) OrErr(args ...any) T {
	panicArgs(m.err, args...)
	return m.val
}

func (m *mustX1[T]) OrErrSlice(args []any) T {
	return m.OrErr(args...)
}

func (m *mustX1[T]) OrErrFn(f func(error)) T {
	panicErr(m.err, f)
	return m.val
}

type mustX2[T1, T2 any] struct {
	val1 T1
	val2 T2
	err  error
}

func MustX2[T1, T2 any](val1 T1, val2 T2, err error) *mustX2[T1, T2] {
	return &mustX2[T1, T2]{
		val1: val1,
		val2: val2,
		err:  err,
	}
}

func (m *mustX2[T1, T2]) OrErr(args ...any) (T1, T2) {
	panicArgs(m.err, args...)
	return m.val1, m.val2
}

func (m *mustX2[T1, T2]) OrErrSlice(args []any) (T1, T2) {
	return m.OrErr(args...)
}

func (m *mustX2[T1, T2]) OrErrFn(f func(error)) (T1, T2) {
	panicErr(m.err, f)
	return m.val1, m.val2
}

type mustX3[T1, T2, T3 any] struct {
	val1 T1
	val2 T2
	val3 T3
	err  error
}

func MustX3[T1, T2, T3 any](val1 T1, val2 T2, val3 T3, err error) *mustX3[T1, T2, T3] {
	return &mustX3[T1, T2, T3]{
		val1: val1,
		val2: val2,
		val3: val3,
		err:  err,
	}
}

func (m *mustX3[T1, T2, T3]) OrErr(args ...any) (T1, T2, T3) {
	panicArgs(m.err, args...)
	return m.val1, m.val2, m.val3
}

func (m *mustX3[T1, T2, T3]) OrErrSlice(args []any) (T1, T2, T3) {
	return m.OrErr(args...)
}

func (m *mustX3[T1, T2, T3]) OrErrFn(f func(error)) (T1, T2) {
	panicErr(m.err, f)
	return m.val1, m.val2
}

type mustX4[T1, T2, T3, T4 any] struct {
	val1 T1
	val2 T2
	val3 T3
	val4 T4
	err  error
}

func MustX4[T1, T2, T3, T4 any](val1 T1, val2 T2, val3 T3, val4 T4, err error) *mustX4[T1, T2, T3, T4] {
	return &mustX4[T1, T2, T3, T4]{
		val1: val1,
		val2: val2,
		val3: val3,
		val4: val4,
		err:  err,
	}
}

func (m *mustX4[T1, T2, T3, T4]) OrErr(args ...any) (T1, T2, T3, T4) {
	panicArgs(m.err, args...)
	return m.val1, m.val2, m.val3, m.val4
}

func (m *mustX4[T1, T2, T3, T4]) OrErrSlice(args []any) (T1, T2, T3, T4) {
	return m.OrErr(args...)
}

func (m *mustX4[T1, T2, T3, T4]) OrErrFn(f func(error)) (T1, T2) {
	panicErr(m.err, f)
	return m.val1, m.val2
}

type mustX5[T1, T2, T3, T4, T5 any] struct {
	val1 T1
	val2 T2
	val3 T3
	val4 T4
	val5 T5
	err  error
}

func MustX5[T1, T2, T3, T4, T5 any](val1 T1, val2 T2, val3 T3, val4 T4, val5 T5, err error) *mustX5[T1, T2, T3, T4, T5] {
	return &mustX5[T1, T2, T3, T4, T5]{
		val1: val1,
		val2: val2,
		val3: val3,
		val4: val4,
		val5: val5,
		err:  err,
	}
}

func (m *mustX5[T1, T2, T3, T4, T5]) OrErr(args ...any) (T1, T2, T3, T4, T5) {
	panicArgs(m.err, args...)
	return m.val1, m.val2, m.val3, m.val4, m.val5
}

func (m *mustX5[T1, T2, T3, T4, T5]) OrErrSlice(args []any) (T1, T2, T3, T4, T5) {
	return m.OrErr(args...)
}

func (m *mustX5[T1, T2, T3, T4, T5]) OrErrFn(f func(error)) (T1, T2) {
	panicErr(m.err, f)
	return m.val1, m.val2
}
