package lox

import (
	"runtime"
)

func TryCatch1[A any](callback func() A, catch func(err any)) (A, bool) {
	ok := false
	var fallbackA A

	TryCatchWithError(func() {
		fallbackA = callback()
		ok = true

	}, catch)

	return fallbackA, ok
}

func TryCatch2[A, B any](callback func() (A, B), catch func(err any)) (A, B, bool) {
	ok := false
	var (
		fallbackA A
		fallbackB B
	)

	TryCatchWithError(func() {
		fallbackA, fallbackB = callback()
		ok = true

	}, catch)

	return fallbackA, fallbackB, ok
}

func TryCatch3[A, B, C any](callback func() (A, B, C), catch func(err any)) (A, B, C, bool) {
	ok := false
	var (
		fallbackA A
		fallbackB B
		fallbackC C
	)

	TryCatchWithError(func() {
		fallbackA, fallbackB, fallbackC = callback()
		ok = true

	}, catch)

	return fallbackA, fallbackB, fallbackC, ok
}

func TryCatch4[A, B, C, D any](callback func() (A, B, C, D), catch func(err any)) (A, B, C, D, bool) {
	ok := false
	var (
		fallbackA A
		fallbackB B
		fallbackC C
		fallbackD D
	)

	TryCatchWithError(func() {
		fallbackA, fallbackB, fallbackC, fallbackD = callback()
		ok = true

	}, catch)

	return fallbackA, fallbackB, fallbackC, fallbackD, ok
}

func TryCatch5[A, B, C, D, E any](callback func() (A, B, C, D, E), catch func(err any)) (A, B, C, D, E, bool) {
	ok := false
	var (
		fallbackA A
		fallbackB B
		fallbackC C
		fallbackD D
		fallbackE E
	)

	TryCatchWithError(func() {
		fallbackA, fallbackB, fallbackC, fallbackD, fallbackE = callback()
		ok = true

	}, catch)

	return fallbackA, fallbackB, fallbackC, fallbackD, fallbackE, ok
}

func TryCatch6[A, B, C, D, E, F any](callback func() (A, B, C, D, E, F), catch func(err any)) (A, B, C, D, E, F, bool) {
	ok := false
	var (
		fallbackA A
		fallbackB B
		fallbackC C
		fallbackD D
		fallbackE E
		fallbackF F
	)

	TryCatchWithError(func() {
		fallbackA, fallbackB, fallbackC, fallbackD, fallbackE, fallbackF = callback()
		ok = true

	}, catch)

	return fallbackA, fallbackB, fallbackC, fallbackD, fallbackE, fallbackF, ok
}

func TryCatchWithError(callback func(), catch func(any)) {
	if err, ok := TryWithError(callback); !ok {
		catch(err)
	}
}

func TryWithError(callback func()) (errorValue any, ok bool) {
	ok = true

	defer func() {
		if r := recover(); r != nil {
			ok = false
			errorValue = r

			switch r.(type) {
			case runtime.Error:
				printCallerStack()
			}
		}
	}()

	callback()
	return
}

func Try(callback func()) (ok bool) {
	ok = true

	defer func() {
		if r := recover(); r != nil {
			ok = false

			switch r.(type) {
			case runtime.Error:
				printCallerStack()
			}
		}
	}()

	callback()
	return
}

func Try0(callback func()) bool {
	return Try(callback)
}

func Try1[A any](callback func() A) (A, bool) {
	ok := false
	var fallbackA A

	Try0(func() {
		fallbackA = callback()
		ok = true
	})

	return fallbackA, ok
}

func Try2[A, B any](callback func() (A, B)) (A, B, bool) {
	ok := false
	var (
		fallbackA A
		fallbackB B
	)

	Try0(func() {
		fallbackA, fallbackB = callback()
		ok = true
	})

	return fallbackA, fallbackB, ok
}

func Try3[A, B, C any](callback func() (A, B, C)) (A, B, C, bool) {
	ok := false
	var (
		fallbackA A
		fallbackB B
		fallbackC C
	)

	Try0(func() {
		fallbackA, fallbackB, fallbackC = callback()
		ok = true
	})

	return fallbackA, fallbackB, fallbackC, ok
}

func Try4[A, B, C, D any](callback func() (A, B, C, D)) (A, B, C, D, bool) {
	ok := false
	var (
		fallbackA A
		fallbackB B
		fallbackC C
		fallbackD D
	)

	Try0(func() {
		fallbackA, fallbackB, fallbackC, fallbackD = callback()
		ok = true
	})

	return fallbackA, fallbackB, fallbackC, fallbackD, ok
}

func Try5[A, B, C, D, E any](callback func() (A, B, C, D, E)) (A, B, C, D, E, bool) {
	ok := false
	var (
		fallbackA A
		fallbackB B
		fallbackC C
		fallbackD D
		fallbackE E
	)

	Try0(func() {
		fallbackA, fallbackB, fallbackC, fallbackD, fallbackE = callback()
		ok = true
	})

	return fallbackA, fallbackB, fallbackC, fallbackD, fallbackE, ok
}

func Try6[A, B, C, D, E, F any](callback func() (A, B, C, D, E, F)) (A, B, C, D, E, F, bool) {
	ok := false
	var (
		fallbackA A
		fallbackB B
		fallbackC C
		fallbackD D
		fallbackE E
		fallbackF F
	)

	Try0(func() {
		fallbackA, fallbackB, fallbackC, fallbackD, fallbackE, fallbackF = callback()
		ok = true
	})

	return fallbackA, fallbackB, fallbackC, fallbackD, fallbackE, fallbackF, ok
}

func TryCatch(callback func(), catch func()) {
	if !Try(callback) {
		catch()
	}
}
