package lox

import "fmt"

func Log(val any) func(any) []any {
	return func(val1 any) []any {
		return []any{val, val1}
	}
}

func Log2(val any) func(any, any) []any {
	return func(val1, val2 any) []any {
		return []any{val, fmt.Sprintf("%+v: %+v", val1, val2)}
	}
}

func Log3(val any) func(any, any, any) []any {
	return func(val1, val2, val3 any) []any {
		return []any{val, fmt.Sprintf("%+v: %+v: %+v", val1, val2, val3)}
	}
}

func Log4(val any) func(any, any, any, any) []any {
	return func(val1, val2, val3, val4 any) []any {
		return []any{val, fmt.Sprintf("%+v: %+v: %+v: %+v", val1, val2, val3, val4)}
	}
}

func LogX1(val any) func(any) []any {
	return func(val1 any) []any {
		return []any{val, val1}
	}
}

func LogX2(val any) func(any) func(any) []any {
	return func(val1 any) func(any) []any {
		return func(val2 any) []any {
			return []any{val, fmt.Sprintf("%+v: %+v: %+v", val, val2, val2)}
		}
	}
}

func LogX3(val any) func(any) func(any) func(any) []any {
	return func(val1 any) func(any) func(any) []any {
		return func(val2 any) func(any) []any {
			return func(val3 any) []any {
				return []any{val, fmt.Sprintf("%+v: %+v: %+v: %+v", val, val1, val2, val3)}
			}
		}
	}
}
