package lox

import (
	"fmt"
	"net/http"
	"testing"
	"time"
)

func ax2() (int, string, error) {
	return 1, "1", fmt.Errorf("error2")
}

func TestWrap2_Err(t *testing.T) {
	TryCatchWithError(func() {
		i, s := MustX2(ax2()).OrErr()
		t.Log(i, s)
	}, func(err any) {
		if er, ok := UnErr0(err); ok {
			fmt.Println(er)
		} else if i, s, er, ok := UnErr2[int, string](err); ok {
			fmt.Println(i, s, er)
		}
	})
}

func TestWrap2_ErrFn(t *testing.T) {
	Try(func() {
		i, s := MustX2(ax2()).OrErrFn(func(err2 error) {
			fmt.Println(err2)
		})
		t.Log(i, s)
	})
}

func ax5() (int, string, bool, int, string, error) {
	return 1, "2", true, 3, "4", fmt.Errorf("error5")
}

func TestWrap5_Err(t *testing.T) {
	ch := make(chan func() (int, string, bool, int, string, error), 10)

	f := func() {
		for {
			select {
			case ax5f := <-ch:
				TryCatchWithError(func() {
					v1, v2, v3, v4, v5 := MustX5(ax5f()).OrErr(http.StatusBadRequest, "bad request")
					fmt.Println(v1, v2, v3, v4, v5)
				}, func(err any) {
					if code, msg, er, ok := UnErr2[int, string](err); ok {
						fmt.Println(code, msg, er)
					}
				})
			}
		}
	}
	for i := 0; i < 10; i++ {
		go f()
	}

	for i := 0; i < 100; i++ {
		ch <- ax5
	}

	time.Sleep(5 * time.Second)
}

func unErrArgs1() error {
	return fmt.Errorf("errors")
}

func Test_UnErrArgs(t *testing.T) {
	TryCatchWithError(func() {
		MustX0(unErrArgs1()).OrErr("1", 2, true)
	}, func(err any) {
		args := UnErrArgs(err)
		//t.Log(args)
		fmt.Println(args)
	})
}

func a2() (int, string, error) {
	return 1, "1", fmt.Errorf("error2")
}

var log2 = Log2(400)

func Test_OrErrSlice(t *testing.T) {
	TryCatchWithError(func() {
		i, s := MustX2(a2()).OrErrSlice(log2("a", "b"))
		t.Log(i, s)
	}, func(err any) {
		if code, msg, er, ok := UnErr2[int, string](err); ok {
			t.Log(code, msg, er)
		}
	})
}
