package lox

import (
	"fmt"
	"runtime"
)

func printCallerStack() {
	// 添加一个缓冲区，用来接收调用信息
	const size = 64
	buf := make([]uintptr, size)
	// 跳过前2个调用，第1个是 runtime.Callers()
	n := runtime.Callers(3, buf[:])
	if n == 0 {
		fmt.Println("no caller found")
		return
	}
	// 遍历调用信息
	fmt.Println("Caller Stack (business code):")
	for i := 0; i < n; i++ {
		// 获取调用信息
		pc := buf[i] - 1 // 我们需要通过 pc 信息获取函数名
		fn := runtime.FuncForPC(pc)
		file, line := fn.FileLine(pc)
		// 过滤掉标准库和其他辅助函数的调用链
		if filterSystemFn(i) {
			continue
		}
		// 打印调用行信息
		fmt.Printf("\t%s:%d %s()\n", file, line, fn.Name())
	}
}

func filterSystemFn(i int) bool {
	return i < 3
}

func isBusinessCode(name string) bool {
	if name == "" || name[0] == '(' {
		return false // 忽略任何没有函数名称或以括号开头的调用
	}
	if name == "main.main" || name == "runtime.main" {
		return false // 忽略 main 函数及其内部调用
	}
	if name[:4] == "main" {
		return true // 如果函数名以 main 开头，我们认为它是业务代码
	}
	return false // 其他情况认为是辅助函数或标准库函数
}
