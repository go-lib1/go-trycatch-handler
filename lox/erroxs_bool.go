package lox

import "github.com/samber/lo"

type mustXBool struct {
	ok bool
}

func MustXBool(ok bool) *mustXBool {
	return &mustXBool{
		ok: ok,
	}
}

func (m *mustXBool) OrErr(args ...any) {
	panicArgs(lo.Validate(m.ok, ""), args...)
}

func (m *mustXBool) OrErrSlice(args []any) {
	m.OrErr(args...)
}

func (m *mustXBool) OrErrFn(f func(error)) {
	panicErr(lo.Validate(m.ok, ""), f)
}

type mustXBool1[T any] struct {
	val T
	ok  bool
}

func MustXBool1[T any](val T, ok bool) *mustXBool1[T] {
	return &mustXBool1[T]{
		val: val,
		ok:  ok,
	}
}

func (m *mustXBool1[T]) OrErr(args ...any) T {
	panicArgs(lo.Validate(m.ok, ""), args...)
	return m.val
}

func (m *mustXBool1[T]) OrErrSlice(args []any) T {
	return m.OrErr(args...)
}

func (m *mustXBool1[T]) OrErrFn(f func(error)) T {
	panicErr(lo.Validate(m.ok, ""), f)
	return m.val
}

type mustXBool2[T1, T2 any] struct {
	val1 T1
	val2 T2
	ok   bool
}

func MustXBool2[T1, T2 any](val1 T1, val2 T2, ok bool) *mustXBool2[T1, T2] {
	return &mustXBool2[T1, T2]{
		val1: val1,
		val2: val2,
		ok:   ok,
	}
}

func (m *mustXBool2[T1, T2]) OrErr(args ...any) (T1, T2) {
	panicArgs(lo.Validate(m.ok, ""), args...)
	return m.val1, m.val2
}

func (m *mustXBool2[T1, T2]) OrErrSlice(args []any) (T1, T2) {
	return m.OrErr(args...)
}

func (m *mustXBool2[T1, T2]) OrErrFn(f func(error)) (T1, T2) {
	panicErr(lo.Validate(m.ok, ""), f)
	return m.val1, m.val2
}

type mustXBool3[T1, T2, T3 any] struct {
	val1 T1
	val2 T2
	val3 T3
	ok   bool
}

func MustXBool3[T1, T2, T3 any](val1 T1, val2 T2, val3 T3, ok bool) *mustXBool3[T1, T2, T3] {
	return &mustXBool3[T1, T2, T3]{
		val1: val1,
		val2: val2,
		val3: val3,
		ok:   ok,
	}
}

func (m *mustXBool3[T1, T2, T3]) OrErr(args ...any) (T1, T2, T3) {
	panicArgs(lo.Validate(m.ok, ""), args...)
	return m.val1, m.val2, m.val3
}

func (m *mustXBool3[T1, T2, T3]) OrErrSlice(args []any) (T1, T2, T3) {
	return m.OrErr(args...)
}

func (m *mustXBool3[T1, T2, T3]) OrErrFn(f func(error)) (T1, T2, T3) {
	panicErr(lo.Validate(m.ok, ""), f)
	return m.val1, m.val2, m.val3
}
