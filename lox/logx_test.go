package lox

import (
	"fmt"
	"github.com/samber/lo"
	"testing"
)

func getIntWithError() (int, error) {
	return 1, nil
}

func getStringWithError() (string, error) {
	return "2", nil
}

func getInt() int {
	return lo.Must1(getIntWithError())
}

func getString() string {
	return lo.Must1(getStringWithError())
}

func Test_lox(t *testing.T) {
	idx, name, ok := TryCatch2(func() (int, string) {
		//在外出断言
		i := lo.Must1(getIntWithError())
		s := lo.Must1(getStringWithError())
		//内部断言
		i1 := getInt()
		s1 := getString()
		//可以自己处理错误
		i2, err := getIntWithError()
		if err != nil {
			fmt.Println(err)
			lo.Must0(err)
		}

		return i + i1 + i2, s + s1
	}, func(err any) {
		fmt.Println(err)
	})

	if !ok {
		t.Error("should be ok")
	}

	fmt.Println(idx, name, ok)
}

func Test_git_lo(t *testing.T) {
	a, ok := Try1(func() int {
		return lo.Must1(1, false)
	})
	t.Log(a, ok)
}
