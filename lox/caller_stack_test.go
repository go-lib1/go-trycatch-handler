package lox

import (
	"fmt"
	"testing"
)

type T1 struct {
	t1 *int
}

func Test_stack(t *testing.T) {
	Try(func() {
		//var a *int
		//fmt.Println(*a)

		var t1 = new(T1)
		fmt.Println(*t1.t1)
	})
}
