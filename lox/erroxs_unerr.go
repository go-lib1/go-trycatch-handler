package lox

import "github.com/samber/lo"

func UnErrArgs(err any) []any {
	var args []any
	switch v := err.(type) {
	case error:
		args = append(args, v)
	case lo.Tuple2[error, any]:
		er, b := v.Unpack()
		args = append(args, b, er)
	case lo.Tuple3[error, any, any]:
		er, b, c := v.Unpack()
		args = append(args, b, c, er)
	case lo.Tuple4[error, any, any, any]:
		er, b, c, d := v.Unpack()
		args = append(args, b, c, d, er)
	case lo.Tuple5[error, any, any, any, any]:
		er, b, c, d, e := v.Unpack()
		args = append(args, b, c, d, e, er)
	case lo.Tuple6[error, any, any, any, any, any]:
		er, b, c, d, e, f := v.Unpack()
		args = append(args, b, c, d, e, f, er)
	case lo.Tuple7[error, any, any, any, any, any, any]:
		er, b, c, d, e, f, g := v.Unpack()
		args = append(args, b, c, d, e, f, g, er)
	}
	return args
}

func UnErr0(err any) (error, bool) {
	if er, ok := err.(error); ok {
		return er, true
	}
	return nil, false
}

func UnErr1[T any](err any) (T, error, bool) {
	if t2, ok := err.(lo.Tuple2[error, any]); ok {
		er, valAny := t2.Unpack()
		if val, ok1 := valAny.(T); ok1 {
			return val, er, true
		}
	}
	return lo.Empty[T](), nil, false
}

func UnErr2[T1, T2 any](err any) (T1, T2, error, bool) {
	if t3, ok := err.(lo.Tuple3[error, any, any]); ok {
		er, valAny1, valAny2 := t3.Unpack()
		val1, ok1 := valAny1.(T1)
		val2, ok2 := valAny2.(T2)
		if ok1 && ok2 {
			return val1, val2, er, true
		}
	}
	return lo.Empty[T1](), lo.Empty[T2](), nil, false
}

func UnErr3[T1, T2, T3 any](err any) (T1, T2, T3, error, bool) {
	if t4, ok := err.(lo.Tuple4[error, any, any, any]); ok {
		er, valAny1, valAny2, valAny3 := t4.Unpack()
		val1, ok1 := valAny1.(T1)
		val2, ok2 := valAny2.(T2)
		val3, ok3 := valAny3.(T3)
		if ok1 && ok2 && ok3 {
			return val1, val2, val3, er, true
		}
	}
	return lo.Empty[T1](), lo.Empty[T2](), lo.Empty[T3](), nil, false
}

func UnErr4[T1, T2, T3, T4 any](err any) (T1, T2, T3, T4, error, bool) {
	if t5, ok := err.(lo.Tuple5[error, any, any, any, any]); ok {
		er, valAny1, valAny2, valAny3, valAny4 := t5.Unpack()
		val1, ok1 := valAny1.(T1)
		val2, ok2 := valAny2.(T2)
		val3, ok3 := valAny3.(T3)
		val4, ok4 := valAny4.(T4)
		if ok1 && ok2 && ok3 && ok4 {
			return val1, val2, val3, val4, er, true
		}
	}
	return lo.Empty[T1](), lo.Empty[T2](), lo.Empty[T3](), lo.Empty[T4](), nil, false
}

func UnErr5[T1, T2, T3, T4, T5 any](err any) (T1, T2, T3, T4, T5, error, bool) {
	if t6, ok := err.(lo.Tuple6[error, any, any, any, any, any]); ok {
		er, valAny1, valAny2, valAny3, valAny4, valAny5 := t6.Unpack()
		val1, ok1 := valAny1.(T1)
		val2, ok2 := valAny2.(T2)
		val3, ok3 := valAny3.(T3)
		val4, ok4 := valAny4.(T4)
		val5, ok5 := valAny5.(T5)
		if ok1 && ok2 && ok3 && ok4 && ok5 {
			return val1, val2, val3, val4, val5, er, true
		}
	}
	return lo.Empty[T1](), lo.Empty[T2](), lo.Empty[T3](), lo.Empty[T4](), lo.Empty[T5](), nil, false
}
